/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/26 18:24:47 by psemsari          #+#    #+#             */
/*   Updated: 2020/09/10 17:02:48 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "libasm.h"

int		main(void)
{
	char *str_long = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed adipiscing diam donec adipiscing tristique. Sem integer vitae justo eget. Id volutpat lacus laoreet non curabitur gravida arcu ac tortor. Metus aliquam eleifend mi in nulla posuere sollicitudin aliquam. Sem et tortor consequat id porta nibh venenatis cras sed. Vulputate enim nulla aliquet porttitor lacus. Ut sem nulla pharetra diam sit amet nisl. Cras semper auctor neque vitae tempus quam pellentesque. Pharetra vel turpis nunc eget lorem dolor. Posuere morbi leo urna molestie. Enim eu turpis egestas pretium aenean pharetra magna. Vitae suscipit tellus mauris a diam maecenas sed. Ornare suspendisse sed nisi lacus sed viverra tellus in.";
	int len_long = 771;
	char *str_vide = "";
	int len_vide = 0;

	char *str_ok = "ok";

	char copy_long[771];
	char copy_vide[10];

	int fd;
	int ret;

	printf("ft_strlen :\n");
	printf("long : %d - %d\n", (int)ft_strlen(str_long), len_long - 1); //pas de /0
	printf("vide : %d - %d\n", (int)ft_strlen(str_vide), len_vide);
	printf("strlen :\n");
	printf("long : %d - %d\n", (int)strlen(str_long), len_long - 1); //pas de /0
	printf("vide : %d - %d\n", (int)strlen(str_vide), len_vide);
	printf("\n");

	ft_read(1, "", 1);

	printf("ft_strcpy :\n");
	printf("long : -%s- %d\n", ft_strcpy(copy_long, str_long), (int)strlen(copy_long)); //pas de /0
	printf("vide : -%s- %d\n", ft_strcpy(copy_vide, str_vide), (int)strlen(copy_vide));
	memset(copy_long, 0, len_long);
	memset(copy_vide, 0, 10);
	printf("strcpy :\n");
	printf("long : -%s- %d\n", strcpy(copy_long, str_long), (int)strlen(copy_long)); //pas de /0
	printf("vide : -%s- %d\n", strcpy(copy_vide, str_vide), (int)strlen(copy_vide));
	memset(copy_long, 0, len_long);
	memset(copy_vide, 0, 10);
	printf("\n");

	ft_read(1, "", 1);

	printf("ft_strcmp :\n");
	printf("vide : %d\n", ft_strcmp(str_vide, str_vide));
	printf("vide 1 %d\n", ft_strcmp(str_vide, "ok"));
	printf("vide 2 %d\n", ft_strcmp("ok", str_vide));
	printf("strcmp :");
	printf("attention !\n%d : %d\n", strcmp("", "ok"), strcmp(str_vide, str_ok));
	printf("vide : %d\n", strcmp(str_vide, str_vide));
	printf("vide 1 %d\n", strcmp(str_vide, "ok"));
	printf("vide 2 %d\n", strcmp("ok", str_vide));
	printf("\n");

	ft_read(1, "", 1);

	printf("ft_write\n");
	fd = open("test.txt", O_WRONLY | O_CREAT, 420);
	ret = ft_write(1, str_long, len_long);
	printf("\nret = %d\n", ret);
	printf("write in test.txt\n");
	ret = ft_write(fd, str_long, len_long);
	printf("ret = %d\n", ret);
	printf("bad fd\n");
	ret = ft_write(26, str_long, len_long);
	printf("errno = %d - ret = %d\n", errno, ret);
	close(fd);
	printf("\n");

	ft_read(1, "", 1);

	printf("ft_read\n");
	fd = open("test.txt", O_RDONLY);
	ret = (int)ft_read(0, copy_long, len_long);
	printf("-%s-\nret = %d\n", copy_long, ret);
	memset(copy_long, 0, len_long);
	printf("\nread in test.txt\n");
	ret = (int)ft_read(fd, copy_long, len_long);
	printf("-%s-\nret = %d\n", copy_long, ret);
	printf("bad fd\n");
	ret = (int)ft_read(26, copy_long, len_long);
	printf("errno = %d - ret = %d\n", errno, ret);
	close(fd);
	printf("\n");

	ft_read(1, "", 1);

	printf("ft_strdup :\n");
	printf("long : %-s-\n", ft_strdup(str_long));
	printf("vide : %-s-\n", ft_strdup(str_vide));
	return (0);
}
